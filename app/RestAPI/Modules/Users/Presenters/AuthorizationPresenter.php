<?php

declare(strict_types=1);

namespace RestAPI\UsersModule\Presenters;

use Packeto\RestRouter\Application\UI\ResourcePresenter;
use Packeto\RestRouter\Common\Annotations\ApiRoute;
use Packeto\RestRouter\Common\Annotations\Authenticated;
use Packeto\RestRouter\Common\Annotations\DTO;
use Packeto\RestRouter\Common\Annotations\JsonSchema;
use Packeto\RestRouter\Exception\Api\ClientErrorException;
use RestAPI\Users\Api\DTO\LoginPostDTO;
use RestAPI\Users\Api\DTO\RegisterPostDTO;
use RestAPI\Users\Api\Response\AuthenticationResponseBuilder;
use RestAPI\Users\Model\IUserProvider;
use RestAPI\Users\Model\IUserStorage;


/**
 * @ApiRoute(
 *        "/api/v1/users/auth",
 *        presenter="Users:Authorization"
 * )
 */
class AuthorizationPresenter extends ResourcePresenter
{
	/** @var IUserProvider */
	private $userProvider;

	/** @var IUserStorage */
	private $userStorage;


	public function __construct(
		IUserProvider $userProvider,
		IUserStorage $userStorage
	)
	{
		parent::__construct();

		$this->userProvider = $userProvider;
		$this->userStorage = $userStorage;
	}


	/**
	 * @ApiRoute(
	 * 		"/api/v1/users/auth/register",
	 * 		method="POST"
	 * )
	 * @JsonSchema("user/register.post.json")
	 * @DTO("RestAPI\Users\Api\DTO\RegisterPostDTO")
	 */
	public function actionAuthRegister()
	{
		$dto = $this->getCommandDTO();

		if (!$dto instanceof RegisterPostDTO) {
			throw new \InvalidArgumentException;
		}

		try {
			$user = $this->userStorage->register($dto);

		} catch (\Exception $e) {
			throw new ClientErrorException($e->getMessage(), $e->getCode());
		}

		$this->sendSuccessResponse(AuthenticationResponseBuilder::build($user));
	}


	/**
	 * @ApiRoute(
	 * 		"/api/v1/users/auth/login",
	 * 		method="POST"
	 * )
	 * @JsonSchema("user/login.post.json")
	 * @DTO("RestAPI\Users\Api\DTO\LoginPostDTO")
	 */
	public function actionAuthLogin()
	{
		$dto = $this->getCommandDTO();

		if (!$dto instanceof LoginPostDTO) {
			throw new \InvalidArgumentException;
		}

		try {
			$user = $this->userProvider->login($dto);

		} catch (\Exception $e) {
			throw new ClientErrorException($e->getMessage(), $e->getCode());
		}

		$this->sendSuccessResponse(AuthenticationResponseBuilder::build($user));
	}

}