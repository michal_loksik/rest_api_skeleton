<?php

declare(strict_types=1);

namespace RestAPI\Modules\Providers;

interface IRouterProvider
{

	public function getRouterSettings(): array;

}
