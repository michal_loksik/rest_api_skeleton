<?php

declare(strict_types=1);

namespace RestAPI\Modules\Providers;

interface IPresenterMappingProvider
{

	public function getPresenterMapping(): array;
}
