<?php

declare(strict_types=1);

namespace RestAPI\Users\Api\DTO;

use Packeto\RestRouter\Command\ICommandDTO;

class RegisterPostDTO implements ICommandDTO
{

	/** @var string */
	private $email;

	/** @var string */
	private $password;

	/** @var string */
	private $firstname;

	/** @var string */
	private $lastname;


	public function __construct(
		string $email,
		string $password,
		string $firstname,
		string $lastname
	)
	{
		$this->email = $email;
		$this->password = $password;
		$this->firstname = $firstname;
		$this->lastname = $lastname;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->email,
			$data->password,
			$data->firstname,
			$data->lastname
		);
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	/**
	 * @return string
	 */
	public function getFirstname(): string
	{
		return $this->firstname;
	}

	/**
	 * @return string
	 */
	public function getLastname(): string
	{
		return $this->lastname;
	}

}