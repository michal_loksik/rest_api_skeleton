<?php

declare(strict_types=1);

namespace RestAPI\VehiclesModule\Presenters;

use Nette\Http\IResponse;
use Packeto\RestRouter\Application\UI\ResourcePresenter;
use Packeto\RestRouter\Common\Annotations\ApiRoute;
use Packeto\RestRouter\Common\Annotations\Authenticated;
use Packeto\RestRouter\Common\Annotations\DTO;
use Packeto\RestRouter\Common\Annotations\JsonSchema;
use Packeto\RestRouter\Exception\Api\ClientErrorException;
use RestAPI\Vehicles\Api\Response\VehicleResponseBuilder;
use RestAPI\Vehicles\Model\IVehicleProvider;
use RestAPI\Vehicles\Model\IVehicleStorage;

/**
 * @ApiRoute(
 *        "/api/v1/vehicle",
 *        presenter="Vehicles:Vehicle"
 * )
 */
class VehiclePresenter extends ResourcePresenter
{

	/** @var IVehicleProvider */
	private $vehicleProvider;

	/** @var IVehicleStorage */
	private $vehicleStorage;


	public function __construct(
		IVehicleProvider $vehicleProvider,
		IVehicleStorage $vehicleStorage
	)
	{
		parent::__construct();

		$this->vehicleProvider = $vehicleProvider;
		$this->vehicleStorage = $vehicleStorage;
	}


	/**
	 * @Authenticated
	 * @ApiRoute(
	 *    "/api/v1/vehicle/<vin>",
	 *  method="GET"
	 * )
	 */
	public function actionReadDetail(string $vin)
	{
		$vehicle = $this->vehicleProvider->getByVin($vin);

		if (!$vehicle) {
			throw new ClientErrorException('Vehicle not found', IResponse::S404_NOT_FOUND);
		}

		$this->sendSuccessResponse(VehicleResponseBuilder::build($vehicle));
	}

}