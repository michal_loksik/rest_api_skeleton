<?php

declare(strict_types=1);

namespace RestAPI\Vehicles;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Nette\SmartObject;

/**
 * @ORM\Table(name="manufacturer")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Manufacturer
{

	use SmartObject;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var integer
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $name;

	/**
	 * @var Collection
	 *
	 * @ORM\OneToMany(targetEntity="RestAPI\Vehicles\Model", mappedBy="manufacturer")
	 */
	protected $models;


	public function __construct()
	{
		$this->models = new ArrayCollection();
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return Collection
	 */
	public function getModels(): Collection
	{
		return $this->models;
	}

	/**
	 * @param Collection $models
	 */
	public function setModels(Collection $models): void
	{
		$this->models = $models;
	}

}