<?php

declare(strict_types=1);

namespace RestAPI\Vehicles\Model;

use RestAPI\Vehicles\Vehicle;

interface IVehicleProvider
{

	public function getByVin(string $vin): ?Vehicle;

}