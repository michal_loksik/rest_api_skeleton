<?php

declare(strict_types=1);

namespace RestAPI\Users\Model;

use RestAPI\Users\Api\DTO\RegisterPostDTO;
use RestAPI\Users\User;

interface IUserStorage
{

	public function register(RegisterPostDTO $dto): User;

}