<?php

declare(strict_types=1);

namespace RestAPI\UsersModule\Presenters;

use Packeto\RestRouter\Application\UI\ResourcePresenter;
use Packeto\RestRouter\Common\Annotations\ApiRoute;
use Packeto\RestRouter\Common\Annotations\Authenticated;
use Packeto\RestRouter\Common\Annotations\DTO;
use Packeto\RestRouter\Common\Annotations\JsonSchema;
use Packeto\RestRouter\Exception\Api\ClientErrorException;
use RestAPI\Users\Api\Response\UserResponseBuilder;
use RestAPI\Users\Model\IUserProvider;
use RestAPI\Users\Model\IUserStorage;
use RestAPI\Users\User;


/**
 * @ApiRoute(
 *        "/api/v1/user",
 *        presenter="Users:User"
 * )
 */
class UserPresenter extends ResourcePresenter
{
	/** @var IUserProvider */
	private $userProvider;

	/** @var IUserStorage */
	private $userStorage;


	public function __construct(
		IUserProvider $userProvider,
		IUserStorage $userStorage
	)
	{
		parent::__construct();

		$this->userProvider = $userProvider;
		$this->userStorage = $userStorage;
	}


	/**
	 * @Authenticated
	 * @ApiRoute(
	 *    "/api/v1/user",
	 *  method="GET"
	 * )
	 */
	public function actionRead()
	{
		/** @var User $user */
		$user = $this->getAuthenticator()->getAuthenticatedUser();

		$this->sendSuccessResponse(UserResponseBuilder::build($user));
	}

}