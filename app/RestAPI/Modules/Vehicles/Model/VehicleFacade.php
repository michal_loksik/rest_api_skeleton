<?php

declare(strict_types=1);

namespace RestAPI\Vehicles\Model;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette\SmartObject;
use RestAPI\Vehicles\Vehicle;

class VehicleFacade implements IVehicleProvider, IVehicleStorage
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	/** @var EntityRepository */
	private $vehicleRepository;


	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->vehicleRepository = $em->getRepository(Vehicle::class);
	}


	public function getByVin(string $vin): ?Vehicle
	{
		$vehicle = $this->vehicleRepository->findOneBy(['vin' => $vin]);

		return $vehicle;
	}

}