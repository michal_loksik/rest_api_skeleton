<?php

declare(strict_types=1);

namespace RestAPI\Vehicles;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Nette\SmartObject;

/**
 * @ORM\Table(name="model")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Model
{

	use SmartObject;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var integer
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $name;

	/**
	 * @var Manufacturer
	 *
	 * @ORM\ManyToOne(targetEntity="RestAPI\Vehicles\Manufacturer")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id")
	 * })
	 */
	protected $manufacturer;


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return Manufacturer
	 */
	public function getManufacturer(): Manufacturer
	{
		return $this->manufacturer;
	}

	/**
	 * @param Manufacturer $manufacturer
	 */
	public function setManufacturer(Manufacturer $manufacturer): void
	{
		$this->manufacturer = $manufacturer;
	}

}