<?php

declare(strict_types=1);

namespace RestAPI\Users\DI;

use RestAPI\Modules\Providers\IPresenterMappingProvider;
use RestAPI\Modules\Providers\IRouterProvider;
use RestAPI\Router\Router\RouterFactory;
use Nette\DI\CompilerExtension;

class UsersExtension extends CompilerExtension implements IPresenterMappingProvider, IRouterProvider
{

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
	}


	public function getPresenterMapping(): array
	{
		return ['Users' => 'RestAPI\\Users\\*Module\\Presenters\\*Presenter'];
	}


	public function getRouterSettings(): array
	{
		return [
			100 => RouterFactory::class
		];
	}

}
