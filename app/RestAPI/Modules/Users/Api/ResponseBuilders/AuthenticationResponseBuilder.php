<?php

declare(strict_types=1);

namespace RestAPI\Users\Api\Response;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use RestAPI\Users\User;

final class AuthenticationResponseBuilder
{

	public static function build(User $user)
	{
		$signer = new Sha256();
		$expiration = time() + (int) env('JWT_EXPIRATION');

		$token = (new Builder())
			->setIssuer(env('JWT_ISSUER'))
			->setAudience(env('JWT_AUDIENCE'))
			->setId($user->getId(), true)
			->setIssuedAt(time())
			->setNotBefore(time())
			->setExpiration($expiration)
			->set('userId', $user->getId())
			->sign($signer, env('JWT_KEY'))
			->getToken();

		return [
			'access_token' => (string) $token,
			'expires' => (int) $expiration,
			'refresh_token' => $user->getRefreshToken(),
			'user' => UserResponseBuilder::build($user)
		];
	}

}