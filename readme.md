Rest API skeleton
=================

This is a simple REST API skeleton based on Nette framework, Doctrine ORM and Redis cache.


Requirements
------------

- PHP 7.2 or higher
- Redis cache


Installation
------------

	composer install


Make directories `temp/` and `log/` writable.


API endpoints
-------------

[POST] http://restapi.loc/api/v1/users/auth/register

Request:
```
{
	"email": "test@gmail.com",
	"password": "test123",
	"firstname": "Michal",
	"lastname": "Loksik"
}
```

Response:
```
{
    "success": true,
    "data": {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjUifQ.eyJpc3MiOiJodHRwOlwvXC9yZXN0YXBpLmxvYyIsImF1ZCI6Imh0dHA6XC9cL3Jlc3RhcGkubG9jIiwianRpIjoiNSIsImlhdCI6MTU0MDIzMjI5OSwibmJmIjoxNTQwMjMyMjk5LCJleHAiOjE1NDAzMTg2OTksInVzZXJJZCI6NX0.VM0XtWFhieDm_GPOyrfapmRr6EwYSbgULc0gZQnAfW8",
        "expires": 1540318699,
        "refresh_token": "a860634df34fc49ba6a63c7382517c28cf925e21",
        "user": {
            "email": "test@gmail.com",
            "firstname": "Michal",
            "lastname": "Loksik"
        }
    }
}
```

---

[POST] http://restapi.loc/api/v1/users/auth/login

Request:
```
{
	"email": "test@gmail.com",
	"password": "test123"
}
```

Response:
```
{
    "success": true,
    "data": {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjUifQ.eyJpc3MiOiJodHRwOlwvXC9yZXN0YXBpLmxvYyIsImF1ZCI6Imh0dHA6XC9cL3Jlc3RhcGkubG9jIiwianRpIjoiNSIsImlhdCI6MTU0MDIzMjI5OSwibmJmIjoxNTQwMjMyMjk5LCJleHAiOjE1NDAzMTg2OTksInVzZXJJZCI6NX0.VM0XtWFhieDm_GPOyrfapmRr6EwYSbgULc0gZQnAfW8",
        "expires": 1540318699,
        "refresh_token": "a860634df34fc49ba6a63c7382517c28cf925e21",
        "user": {
            "email": "test@gmail.com",
            "firstname": "Michal",
            "lastname": "Loksik"
        }
    }
}
```

---

[GET] http://restapi.loc/api/v1/user

Request:
```
Headers: X-Access-Token
```

Response:
```
{
    "success": true,
    "data": {
        "email": "mloksik@gmail.com",
        "firstname": "Michal",
        "lastname": "Loksik"
    }
}
```

---

[GET] http://restapi.loc/api/v1/vehicle/{vin}

Request:
```
Headers: X-Access-Token
```

Response:
```
{
    "success": true,
    "data": {
        "vin": "abc12345",
        "description": "test car",
        "manufacturer": "Skoda",
        "model": "Octavia"
    }
}
```