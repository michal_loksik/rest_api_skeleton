<?php

declare(strict_types=1);

namespace RestAPI\Modules\Providers;

interface ITranslationProvider
{

	public function getTranslationLocation(): string;

}
