<?php

namespace RestAPI\Users\Api;

use RestAPI\Users\User;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;
use Nette\Http\IRequest;
use Packeto\RestRouter\Exception\Api\AuthenticationException;
use Packeto\RestRouter\Security\IAuthenticator;
use RestAPI\Users\Model\IUserProvider;

class Authenticator implements IAuthenticator
{

	public const AUTHENTICATION_HEADER = 'X-Access-Token';

	/**
	 * @var IUserProvider
	 */
	private $userProvider;

	/**
	 * @var User
	 */
	private $authenticatedUser;


	public function __construct(IUserProvider $userProvider)
	{
		$this->userProvider = $userProvider;
	}


	public static function getUserFromToken(string $token): ?string
	{
		try {
			$decoded = (new Parser())->parse($token);

			return $decoded->getClaim('userId');

		} catch (\Exception $e) {
			return null;
		}
	}


	/**
	 * @param IRequest $request
	 * @return void
	 * @throws AuthenticationException
	 */
	public function authenticate(IRequest $request): void
	{
		if (is_null($request->getHeader(self::AUTHENTICATION_HEADER, null))) {
			throw new AuthenticationException(
				sprintf('Header %s is empty, or is not provided.', self::AUTHENTICATION_HEADER),
				AuthenticationException::MISSING_HEADER_VALUE
			);
		}

		try {
			$decoded = (new Parser())->parse((string) $request->getHeader(self::AUTHENTICATION_HEADER));
		} catch (\Exception $e) {
			throw new AuthenticationException(
				sprintf('Header %s is malformed.', self::AUTHENTICATION_HEADER),
				AuthenticationException::MISSING_HEADER_VALUE
			);
		}

		if ($decoded->isExpired() === true) {
			throw new AuthenticationException(
				'Token is expired.',
				803
			);
		}

		if (empty($decoded)) {
			throw new AuthenticationException(
				sprintf('Header %s is malformed.', self::AUTHENTICATION_HEADER),
				AuthenticationException::MISSING_HEADER_VALUE
			);
		} else {
			try {
				$userId = $decoded->getClaim('userId');
			} catch (\OutOfBoundsException $e) {
				throw new AuthenticationException(
					sprintf('Header %s is malformed.', self::AUTHENTICATION_HEADER),
					AuthenticationException::MISSING_HEADER_VALUE
				);
			}
		}

		$validationData = new ValidationData();
		$validationData->setIssuer(env('JWT_ISSUER'));
		$validationData->setAudience(env('JWT_AUDIENCE'));
		$validationData->setId($userId);

		if (
			$decoded->validate($validationData) === true &&
			$decoded->verify((new Sha256()), env('JWT_KEY')) === true
		) {
			$user = $this->userProvider->getById($userId);

			if (!$user) {
				throw new AuthenticationException(
					sprintf('Header %s contains invalid user.', self::AUTHENTICATION_HEADER),
					AuthenticationException::MISSING_HEADER_VALUE
				);
			}

			$this->authenticatedUser = $user;

			return;

		} else {
			throw new AuthenticationException(
				sprintf('Header %s is malformed.', self::AUTHENTICATION_HEADER),
				AuthenticationException::MISSING_HEADER_VALUE
			);
		}
	}


	/**
	 * @return User
	 */
	public function getAuthenticatedUser(): User
	{
		return $this->authenticatedUser;
	}

}