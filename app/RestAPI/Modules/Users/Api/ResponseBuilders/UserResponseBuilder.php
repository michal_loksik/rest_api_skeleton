<?php

declare(strict_types=1);

namespace RestAPI\Users\Api\Response;

use RestAPI\Users\User;

final class UserResponseBuilder
{

	public static function build(User $user)
	{
		return [
			'email' => $user->getEmail(),
			'firstname' => $user->getFirstname(),
			'lastname' => $user->getLastname(),
		];
	}

}