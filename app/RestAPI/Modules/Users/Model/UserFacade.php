<?php

declare(strict_types=1);

namespace RestAPI\Users\Model;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette\Http\IRequest;
use Nette\Security\IAuthenticator;
use Nette\Security\Passwords;
use Nette\SmartObject;
use RestAPI\Users\Api\DTO\LoginPostDTO;
use RestAPI\Users\Api\DTO\RegisterPostDTO;
use RestAPI\Users\User;

class UserFacade implements IUserProvider, IUserStorage, IAuthenticator
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	/** @var EntityRepository */
	private $userRepository;


	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->userRepository = $this->em->getRepository(User::class);
	}


	public function getById(int $id): ?User
	{
		return $this->userRepository->find($id);
	}


	public function getByEmail(string $email): ?User
	{
		return $this->userRepository->findOneBy(['email' => $email]);
	}


	public function login(LoginPostDTO $dto): User
	{
		$user = $this->authenticate([$dto->getEmail(), $dto->getPassword()]);

		$user->setRefreshToken(sha1(time() . $dto->getEmail()));

		$this->em->persist($user);
		$this->em->flush();

		return $user;
	}


	public function register(RegisterPostDTO $dto): User
	{
		$userExists = $this->userRepository->findOneBy(['email' => $dto->getEmail()]);

		if ($userExists) {
			throw new \Exception('User already exists.');
		}

		$user = new User();

		$user->setEmail($dto->getEmail());
		$user->setPassword(password_hash($dto->getPassword(), PASSWORD_BCRYPT));
		$user->setFirstname($dto->getFirstname());
		$user->setLastname($dto->getLastname());
		$user->setRefreshToken(sha1(time() . $dto->getEmail()));

		$this->em->persist($user);
		$this->em->flush();

		return $user;
	}


	function authenticate(array $credentials): User
	{
		list($email, $password) = $credentials;
		$user = $this->getByEmail($email);

		if (!$user) {
			throw new \Exception('User not found.');
		}

		if (Passwords::verify($password, $user->getPassword())) {
			return $user;
		}

		throw new \Exception('Invalid password.');
	}
}