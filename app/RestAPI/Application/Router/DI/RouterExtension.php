<?php

declare(strict_types=1);

namespace RestAPI\Router\DI;

use RestAPI\Modules\Providers\IRouterProvider;
use RestAPI\Router\Router\RouterFactory;
use Nette\DI\CompilerExtension;

class RouterExtension extends CompilerExtension implements IRouterProvider
{

	public function getRouterSettings(): array
	{
		return [
			100 => RouterFactory::class,
		];
	}

}
