<?php

declare(strict_types=1);

namespace RestAPI\Users;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Nette\SmartObject;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class User
{

	use SmartObject;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var integer
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $email;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $password;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $firstname;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $lastname;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $refreshToken;

	/**
	 * @var Collection
	 *
	 * @ORM\OneToMany(targetEntity="RestAPI\Vehicles\Vehicle", mappedBy="user")
	 */
	protected $vehicles;


	public function __construct()
	{
		$this->vehicles = new ArrayCollection();
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail(string $email): void
	{
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword(string $password): void
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getFirstname(): string
	{
		return $this->firstname;
	}

	/**
	 * @param string $firstname
	 */
	public function setFirstname(string $firstname): void
	{
		$this->firstname = $firstname;
	}

	/**
	 * @return string
	 */
	public function getLastname(): string
	{
		return $this->lastname;
	}

	/**
	 * @param string $lastname
	 */
	public function setLastname(string $lastname): void
	{
		$this->lastname = $lastname;
	}

	/**
	 * @return string
	 */
	public function getRefreshToken(): string
	{
		return $this->refreshToken;
	}

	/**
	 * @param string $refreshToken
	 */
	public function setRefreshToken(string $refreshToken): void
	{
		$this->refreshToken = $refreshToken;
	}

	/**
	 * @return Collection
	 */
	public function getVehicles(): Collection
	{
		return $this->vehicles;
	}

	/**
	 * @param Collection $vehicles
	 */
	public function setVehicles(Collection $vehicles): void
	{
		$this->vehicles = $vehicles;
	}

}