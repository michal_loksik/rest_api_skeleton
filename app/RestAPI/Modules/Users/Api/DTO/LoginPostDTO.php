<?php

declare(strict_types=1);

namespace RestAPI\Users\Api\DTO;

use Packeto\RestRouter\Command\ICommandDTO;

class LoginPostDTO implements ICommandDTO
{

	/** @var string */
	private $email;

	/** @var string */
	private $password;


	public function __construct(
		string $email,
		string $password
	)
	{
		$this->email = $email;
		$this->password = $password;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->email,
			$data->password
		);
	}


	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

}