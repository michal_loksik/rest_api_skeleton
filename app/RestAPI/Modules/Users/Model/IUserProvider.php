<?php

declare(strict_types=1);

namespace RestAPI\Users\Model;

use RestAPI\Users\Api\DTO\LoginPostDTO;
use RestAPI\Users\User;

interface IUserProvider
{

	public function getById(int $id): ?User;


	public function getByEmail(string $email): ?User;


	public function login(LoginPostDTO $dto): User;

}