<?php

declare(strict_types=1);

namespace RestAPI\Modules\DI;

use RestAPI\Modules\Providers\IPresenterMappingProvider;
use RestAPI\Modules\Providers\IRouterProvider;
use RestAPI\Modules\Providers\ITranslationProvider;
use Kdyby\Translation\Translator;
use Nette\DI\CompilerExtension;
use Nette\DI\ServiceDefinition;
use Nette\DI\Statement;
use Nette\Utils\Finder;

class ModulesExtension extends CompilerExtension
{

	public const TAG_ROUTER = 'hoppygo.modules.router';


	public function loadConfiguration(): void
	{
		$builder = $this->getContainerBuilder();

		foreach ($this->compiler->getExtensions() as $extension) {
			if ($extension instanceof IPresenterMappingProvider) {
				$this->setupPresenterMapping($extension);
			}

			if ($extension instanceof ITranslationProvider) {
				$this->setupTranslation($extension);
			}

			if ($extension instanceof IRouterProvider) {
				$this->setupRouter($extension);
			}
		}

		$router = $builder->getDefinition('router');

		foreach ($builder->findByTag(self::TAG_ROUTER) as $serviceName => $priority) {
			if (is_bool($priority)) {
				$priority = 100;
			}

			$routerFactories[$priority][$serviceName] = $serviceName;
		}

		if (!empty($routerFactories)) {
			krsort($routerFactories, \SORT_NUMERIC);

			foreach ($routerFactories as $priority => $items) {
				$routerFactories[$priority] = $items;
			}

			foreach ($routerFactories as $priority => $items) {
				foreach ($items as $serviceName) {
					$factory = new Statement(['@' . $serviceName, 'createRouter']);
					$router->addSetup('offsetSet', [null, $factory]);
				}
			}
		}
	}


	private function setupRouter(IRouterProvider $extension): void
	{
		$builder = $this->getContainerBuilder();

		foreach ($extension->getRouterSettings() as $priority => $router) {
			$builder->addDefinition($this->prefix($extension->name . '.router.' . $priority))
				->setFactory($router)
				->addTag(self::TAG_ROUTER, $priority);
		}
	}


	private function setupPresenterMapping(IPresenterMappingProvider $extension): void
	{
		$builder = $this->getContainerBuilder();

		/** @var ServiceDefinition $presenterFactory */
		$presenterFactory = $builder->getDefinition('application.presenterFactory');
		$presenterFactory->addSetup(
			'setMapping',
			[$extension->getPresenterMapping()]
		);
	}
}
