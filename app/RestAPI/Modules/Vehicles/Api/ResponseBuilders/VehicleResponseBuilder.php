<?php

declare(strict_types=1);

namespace RestAPI\Vehicles\Api\Response;

use RestAPI\Vehicles\Vehicle;

final class VehicleResponseBuilder
{

	public static function build(Vehicle $vehicle)
	{
		return [
			'vin' => $vehicle->getVin(),
			'description' => $vehicle->getDescription(),
			'manufacturer' => $vehicle->getManufacturer()->getName(),
			'model' => $vehicle->getModel()->getName()
		];
	}

}