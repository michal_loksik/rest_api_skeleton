<?php

declare(strict_types=1);

namespace RestAPI\Vehicles;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Nette\SmartObject;
use RestAPI\Users\User;

/**
 * @ORM\Table(name="vehicle")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Vehicle
{

	use SmartObject;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var integer
	 */
	protected $id;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected $vin;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $description;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="RestAPI\Users\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * })
	 */
	protected $user;

	/**
	 * @var Manufacturer
	 *
	 * @ORM\ManyToOne(targetEntity="RestAPI\Vehicles\Manufacturer")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id")
	 * })
	 */
	protected $manufacturer;

	/**
	 * @var Model
	 *
	 * @ORM\ManyToOne(targetEntity="RestAPI\Vehicles\Model")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="model_id", referencedColumnName="id")
	 * })
	 */
	protected $model;


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getVin(): string
	{
		return $this->vin;
	}

	/**
	 * @param string $vin
	 */
	public function setVin(string $vin): void
	{
		$this->vin = $vin;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description): void
	{
		$this->description = $description;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser(User $user): void
	{
		$this->user = $user;
	}

	/**
	 * @return Manufacturer
	 */
	public function getManufacturer(): Manufacturer
	{
		return $this->manufacturer;
	}

	/**
	 * @param Manufacturer $manufacturer
	 */
	public function setManufacturer(Manufacturer $manufacturer): void
	{
		$this->manufacturer = $manufacturer;
	}

	/**
	 * @return Model
	 */
	public function getModel(): Model
	{
		return $this->model;
	}

	/**
	 * @param Model $model
	 */
	public function setModel(Model $model): void
	{
		$this->model = $model;
	}

}